# modifications

This repository contains patches we have applied to services we host. These are usually theme additions but sometimes might include other stuff.

Note: Each subdirectory has a COPYING file which specifies the license our changes are given under. If there isn't one, then the code is automatically licensed under the same license as the original project
